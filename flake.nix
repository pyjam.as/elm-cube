{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    mkElmDerivation.url = "github:jeslie0/mkElmDerivation";
  };

  outputs = { self, nixpkgs, mkElmDerivation }:
    let
      system = "x86_64-linux";

      pkgs = import nixpkgs {
        overlays = [ mkElmDerivation.overlays.mkElmDerivation ];
        inherit system;
      };

      devScript = pkgs.writeShellScriptBin "dev" ''
        cp ${ganWebBluetooth}/esm/index.mjs ./js/gan-web-bluetooth.js
        ${pkgs.elmPackages.elm-live}/bin/elm-live ./src/Main.elm --start-page=./index.html -- --debug --output=js/main.js
      '';

      ganWebBluetooth = import ./gan-web-bluetooth pkgs;

    in {
      packages.${system} = {
        default = pkgs.mkElmDerivation {
          name = "cube experiment";
          src = ./.;
          elmJson = ./elm.json; # This defaults to ${src}/elm.json
          nativeBuildInputs = [ pkgs.elmPackages.elm ];
          buildPhase = ''
            elm make src/Main.elm --output main.js --optimize
          '';
          installPhase = ''
            mkdir -p $out/js
            cp ./main.js $out/js
            cp ${ganWebBluetooth}/esm/index.mjs $out/js/gan-web-bluetooth.js
            cp ./index.html $out
          '';
        };

        gwb = ganWebBluetooth;
      };

      devShells.${system}.default = pkgs.mkShell {
        packages = [
          devScript
          pkgs.elmPackages.elm
          pkgs.elmPackages.elm-format
          pkgs.elmPackages.elm-live
          pkgs.elmPackages.elm-analyse
        ];
      };
    };
}
