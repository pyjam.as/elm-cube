pkgs:
let
  npmlock2nix = import (pkgs.fetchFromGitHub {
    owner = "tweag";
    repo = "npmlock2nix";
    rev = "9197bbf397d76059a76310523d45df10d2e4ca81";
    sha256 = "sha256-sJM82Sj8yfQYs9axEmGZ9Evzdv/kDcI9sddqJ45frrU=";
  }) { inherit pkgs; };

  ganWebBluetoothSrc = pkgs.fetchFromGitHub {
    owner = "afedotov";
    repo = "gan-web-bluetooth";
    rev = "4b82baa1ee9f10491a6077fe8a0a3f3b5266926e";
    sha256 = "sha256-KV/5odw1U7e6uNbvfgGfgFA4Y0Zn7iluRJ4sB1Ovqm0=";
  };

  rollup-config-js = pkgs.writeText "$out/rollup.config.js" ''
    const pkg = require('./package.json');
    const typescript = require('@rollup/plugin-typescript');
    const resolve = require('@rollup/plugin-node-resolve');
    const commonjs = require('@rollup/plugin-commonjs');

    module.exports = {
        input: 'src/index.ts',
        output: [
            {
                file: "dist/esm/index.mjs",
                format: "esm",
            },
            {
                file: "dist/cjs/index.cjs",
                format: "cjs",
            },
            {
                file: "dist/iife/index.js",
                format: "iife",
                name: "ganWebBluetooth",
            }
        ],
        plugins: [
          typescript(),
          resolve({
            // use "jsnext:main" if possible
            // see https://github.com/rollup/rollup/wiki/jsnext:main
            jsnext: true,
            main: true,
            browser: true,
            extensions: [".mjs", ".js", ".ts", ".json"]
          }),
          // Convert CommonJS modules to ES6
          commonjs()
        ],
    };
  '';

  ganWebBluetooth = npmlock2nix.v2.build {
    src = ganWebBluetoothSrc;
    nodejs = pkgs.nodejs_20;
    buildCommands = [
      "cp ${rollup-config-js} ./rollup.config.js"
      "rollup -c --logLevel debug"
    ];
    installPhase = ''
      cp -r ./dist $out
    '';
    node_modules_attrs = {
      packageJson = ./package.json;
      packageLockJson = ./package-lock.json;
    };
  };
in ganWebBluetooth
