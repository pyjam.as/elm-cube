module Cube exposing
    ( Cube
    , CubeSide
    , Face(..)
    , Move(..)
    , TurnDirection(..)
    , emptyCube
    , faceFromChar
    , faceToColor
    , map
    , map2
    , mapSides
    , moveFromString
    , moves
    , solvedCube
    , toList
    )

import Color
import Vector9


type Move
    = Move Face TurnDirection


moves =
    { u = Move U Clockwise
    , uPrime = Move U CounterClockwise
    , l = Move L Clockwise
    , lPrime = Move L CounterClockwise
    , f = Move F Clockwise
    , fPrime = Move F CounterClockwise
    , r = Move R Clockwise
    , rPrime = Move R CounterClockwise
    , b = Move B Clockwise
    , bPrime = Move B CounterClockwise
    , d = Move D Clockwise
    , dPrime = Move D CounterClockwise
    }


{-| Try to parse a Move from common algorithm notation like U or R'
-}
moveFromString : String -> Result String Move
moveFromString str =
    let
        chars =
            String.toList str

        faceR =
            case chars of
                faceChar :: _ ->
                    faceFromChar faceChar

                _ ->
                    Err "String was empty"

        dirR =
            case chars of
                [ _, '\'' ] ->
                    Ok CounterClockwise

                [ _ ] ->
                    Ok Clockwise

                _ ->
                    Err "String too short"
    in
    Result.map2 Move faceR dirR


type TurnDirection
    = Clockwise
    | CounterClockwise


type
    Face
    -- the six faces of a rubik's cube
    = U
    | L
    | F
    | R
    | B
    | D


type alias CubeSide a =
    Vector9.Vector9 a


type alias Cube a =
    { u : CubeSide a
    , l : CubeSide a
    , f : CubeSide a
    , r : CubeSide a
    , b : CubeSide a
    , d : CubeSide a
    }


emptyCube : Cube (Maybe Face)
emptyCube =
    { u = Vector9.repeat Nothing
    , l = Vector9.repeat Nothing
    , f = Vector9.repeat Nothing
    , r = Vector9.repeat Nothing
    , b = Vector9.repeat Nothing
    , d = Vector9.repeat Nothing
    }


solvedCube : Cube Face
solvedCube =
    { u = Vector9.repeat U
    , l = Vector9.repeat L
    , f = Vector9.repeat F
    , r = Vector9.repeat R
    , b = Vector9.repeat B
    , d = Vector9.repeat D
    }


mapSides : (CubeSide a -> CubeSide b) -> Cube a -> Cube b
mapSides fun cube =
    { u = fun cube.u
    , l = fun cube.l
    , f = fun cube.f
    , r = fun cube.r
    , b = fun cube.b
    , d = fun cube.d
    }


map : (a -> b) -> Cube a -> Cube b
map fun cube =
    mapSides (Vector9.map fun) cube


map2 : (a -> b -> c) -> Cube a -> Cube b -> Cube c
map2 fun cubeA cubeB =
    let
        do : CubeSide a -> CubeSide b -> CubeSide c
        do sideA sideB =
            Vector9.map2 fun sideA sideB
    in
    { u = do cubeA.u cubeB.u
    , l = do cubeA.l cubeB.l
    , f = do cubeA.f cubeB.f
    , r = do cubeA.r cubeB.r
    , b = do cubeA.b cubeB.b
    , d = do cubeA.d cubeB.d
    }


toList : Cube a -> List a
toList { u, l, f, r, b, d } =
    -- makes the cube into a big fat list
    -- XXX: note that the order here is Spiffz, and not the order used in the GAN protocol
    Vector9.toList u
        ++ Vector9.toList l
        ++ Vector9.toList f
        ++ Vector9.toList r
        ++ Vector9.toList b
        ++ Vector9.toList d


faceFromChar : Char -> Result String Face
faceFromChar c =
    case c of
        'U' ->
            Ok U

        'L' ->
            Ok L

        'F' ->
            Ok F

        'R' ->
            Ok R

        'B' ->
            Ok B

        'D' ->
            Ok D

        _ ->
            Err <| "Unknown face string: " ++ String.fromChar c


faceToColor : Face -> Color.Color
faceToColor face =
    case face of
        U ->
            Color.white

        L ->
            Color.orange

        F ->
            Color.green

        R ->
            Color.red

        B ->
            Color.blue

        D ->
            Color.yellow
