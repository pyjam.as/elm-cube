module Game exposing
    ( Game
    , GameTimeMillis
    , Level
    , LevelSelect
    , TargetCube
    , TimedTarget(..)
    , countTargets
    , gametime
    , isFinished
    , selectNext
    , selectPrevious
    , startGame
    , updateGame
    )

import Cube exposing (Cube, Face(..))
import Time


type alias TargetCube =
    Cube (Maybe Face)


type alias GameTimeMillis =
    Int


type TimedTarget
    = TimedTarget GameTimeMillis TargetCube


type alias Level =
    { title : String
    , targets : List TimedTarget
    }


type alias Game =
    { startTime : Time.Posix
    , level : Level
    , goodCount : Int
    , badCount : Int
    }


startGame : Time.Posix -> Level -> Game
startGame now level =
    { startTime = now
    , level = level
    , goodCount = 0
    , badCount = 0
    }


gametime : Game -> Time.Posix -> GameTimeMillis
gametime game now =
    Time.posixToMillis now - Time.posixToMillis game.startTime


isFinished : Game -> Bool
isFinished game =
    game.level.targets == []


countTargets : TargetCube -> Cube Face -> ( Int, Int )
countTargets targetCube currentCube =
    let
        comparisonCube : Cube (Maybe Bool)
        comparisonCube =
            Cube.map2
                (\targetFacelet solvedFacelet -> targetFacelet |> Maybe.map ((==) solvedFacelet))
                targetCube
                currentCube
    in
    comparisonCube
        |> Cube.toList
        |> List.foldl
            (\element ( good, bad ) ->
                case element of
                    Just True ->
                        ( good + 1, bad )

                    Just False ->
                        ( good, bad + 1 )

                    Nothing ->
                        ( good, bad )
            )
            ( 0, 0 )


updateGame : Time.Posix -> Cube Face -> Game -> Game
updateGame now currentCube game =
    let
        level =
            game.level

        ( updatedLevel, newGoodCount, newBadCount ) =
            case level.targets of
                (TimedTarget targetTime targetCube) :: rest ->
                    if gametime game now > targetTime then
                        let
                            ( good, bad ) =
                                countTargets targetCube currentCube
                        in
                        ( { level | targets = rest }, good, bad )

                    else
                        ( level, 0, 0 )

                [] ->
                    ( level, 0, 0 )
    in
    { game
        | level = updatedLevel
        , goodCount = game.goodCount + newGoodCount
        , badCount = game.badCount + newBadCount
    }



-- LEVEL SELECT
-- this is just a zip list


type alias LevelSelect =
    { before : List Level
    , selected : Level
    , after : List Level
    }


selectNext : LevelSelect -> LevelSelect
selectNext ls =
    case ls.after of
        nextElement :: _ ->
            { before = ls.before ++ [ ls.selected ]
            , selected = nextElement
            , after = List.drop 1 ls.after
            }

        [] ->
            ls


selectPrevious : LevelSelect -> LevelSelect
selectPrevious ls =
    case ls.before |> List.reverse |> List.head of
        Just prevElement ->
            { before = ls.before |> List.reverse |> List.drop 1 |> List.reverse
            , selected = prevElement
            , after = ls.selected :: ls.after
            }

        Nothing ->
            ls
