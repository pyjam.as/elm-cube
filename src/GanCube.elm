port module GanCube exposing
    ( CubeEvent(..)
    , GyroEvent
    , MoveEvent
    , connectCube
    , cubeFromEvent
    , defaultGyroEvent
    , getCubeEvents
    , requestFacelets
    , requestReset
    )

import Array
import Cube exposing (Cube, Face(..))
import Json.Decode as JD
import Json.Encode as JE
import Quaternion exposing (Quaternion)
import Result.Extra
import Vector12
import Vector54
import Vector8
import Vector9



-- PORTS


port ganCubeEvent : (JD.Value -> msg) -> Sub msg


port connectCube : () -> Cmd msg


port sendCubeCommand : JE.Value -> Cmd msg



-- SUBSCRIBE TO EVENTS


getCubeEvents : (Result JD.Error CubeEvent -> msg) -> Sub msg
getCubeEvents msg =
    ganCubeEvent (JD.decodeValue decodeCubeEvent >> msg)



-- COMMANDS


requestReset : Cmd msg
requestReset =
    sendCubeCommand <| JE.object [ ( "type", JE.string "REQUEST_RESET" ) ]


requestFacelets : Cmd msg
requestFacelets =
    sendCubeCommand <| JE.object [ ( "type", JE.string "REQUEST_FACELETS" ) ]



-- GAN PROTOCOL TYPES


type CubeEvent
    = -- TODO: implement the rest of the events
      --| Battery
      --| Hardware
      --| Disconnect
      Move MoveEvent
    | Facelets FaceletsEvent
    | Gyro GyroEvent


decodeCubeEvent : JD.Decoder CubeEvent
decodeCubeEvent =
    JD.field "type" JD.string
        |> JD.andThen
            (\typestr ->
                case typestr of
                    "MOVE" ->
                        JD.map Move decodeMoveEvent

                    "FACELETS" ->
                        JD.map Facelets decodeFaceletsEvent

                    "GYRO" ->
                        JD.map Gyro decodeGyroEvent

                    _ ->
                        JD.fail ("Unknown event type: " ++ typestr)
            )


type alias MoveEvent =
    { -- /** Face: 0 - U, 1 - R, 2 - F, 3 - D, 4 - L, 5 - B */
      face : Cube.Face
    , -- /** Face direction: 0 - CW, 1 - CCW */
      direction : Cube.TurnDirection
    , -- /** Cube move in common string notation, like R' or U */
      move : Cube.Move
    , -- /** Timestamp according to host device clock, null in case if bluetooth event was missed and recovered */
      localTimestamp : Maybe Int
    , -- /** Timestamp according to cube internal clock */
      cubeTimestamp : Int

    -- everything has an event timestamp i guess
    , timestamp : Int
    }


decodeMoveEventFace : JD.Decoder Cube.Face
decodeMoveEventFace =
    JD.int
        |> JD.andThen
            (\i ->
                case i of
                    0 ->
                        JD.succeed U

                    1 ->
                        JD.succeed R

                    2 ->
                        JD.succeed F

                    3 ->
                        JD.succeed D

                    4 ->
                        JD.succeed L

                    5 ->
                        JD.succeed B

                    _ ->
                        JD.fail <| "Unknown face number in move event: " ++ String.fromInt i
            )


decodeMoveEventTurnDirection : JD.Decoder Cube.TurnDirection
decodeMoveEventTurnDirection =
    JD.int
        |> JD.andThen
            (\dir ->
                case dir of
                    0 ->
                        JD.succeed Cube.Clockwise

                    1 ->
                        JD.succeed Cube.CounterClockwise

                    _ ->
                        JD.fail <| "Unknown turn direction in move event: " ++ String.fromInt dir
            )


decodeMoveEventMove : JD.Decoder Cube.Move
decodeMoveEventMove =
    JD.string
        |> JD.andThen
            (\str ->
                case Cube.moveFromString str of
                    Ok move ->
                        JD.succeed move

                    Err errmsg ->
                        JD.fail <| "Error decoding move string: '" ++ str ++ "' - " ++ errmsg
            )


decodeMoveEvent : JD.Decoder MoveEvent
decodeMoveEvent =
    JD.map6
        (\face direction move localTimestamp cubeTimestamp timestamp ->
            { face = face
            , direction = direction
            , move = move
            , localTimestamp = localTimestamp
            , cubeTimestamp = cubeTimestamp
            , timestamp = timestamp
            }
        )
        (JD.field "face" decodeMoveEventFace)
        (JD.field "direction" decodeMoveEventTurnDirection)
        (JD.field "move" decodeMoveEventMove)
        (JD.field "localTimestamp" (JD.nullable JD.int))
        (JD.field "cubeTimestamp" JD.int)
        (JD.field "timestamp" JD.int)


type alias FaceletsEventState =
    { -- corner position (0-7)
      cp : Vector8.Vector8 Int
    , -- corner orientation (0-2)
      co : Vector8.Vector8 Int
    , -- edge positions (0-11)
      ep : Vector12.Vector12 Int
    , -- edge orientation (0-2)
      eo : Vector12.Vector12 Int
    }


decodeFaceletsEventState : JD.Decoder FaceletsEventState
decodeFaceletsEventState =
    let
        vecDecoder : String -> (List Int -> Maybe ( List a, b )) -> JD.Decoder b
        vecDecoder key vecFun =
            JD.field key
                (JD.array JD.int
                    |> JD.map Array.toList
                    |> JD.map vecFun
                    |> JD.andThen
                        (\maybeVec ->
                            case maybeVec of
                                Just ( [], vec ) ->
                                    JD.succeed vec

                                Just ( _, _ ) ->
                                    JD.fail <| key ++ " array in facelet state is longer than expected"

                                Nothing ->
                                    JD.fail <| key ++ " array in facelet state was shorter than expected"
                        )
                )
    in
    JD.map4
        (\cp co ep eo ->
            { cp = cp
            , co = co
            , ep = ep
            , eo = eo
            }
        )
        (vecDecoder "CP" Vector8.fromList)
        (vecDecoder "CO" Vector8.fromList)
        (vecDecoder "EP" Vector12.fromList)
        (vecDecoder "EO" Vector12.fromList)


type alias FaceletsEvent =
    { facelets : Vector54.Vector54 Cube.Face
    , state : FaceletsEventState
    , timestamp : Int
    }


decodeFaceletsArray : JD.Decoder (Vector54.Vector54 Cube.Face)
decodeFaceletsArray =
    JD.string
        |> JD.map String.toList
        |> JD.andThen
            (\chars ->
                case List.map Cube.faceFromChar chars |> Result.Extra.partition of
                    ( faces, [] ) ->
                        JD.succeed faces

                    ( _, error :: _ ) ->
                        JD.fail error
            )
        |> JD.andThen
            (\faces ->
                case Vector54.fromList faces of
                    Just ( [], vec ) ->
                        JD.succeed vec

                    _ ->
                        JD.fail "facelets string had bad length"
            )


decodeFaceletsEvent : JD.Decoder FaceletsEvent
decodeFaceletsEvent =
    JD.map3 FaceletsEvent
        (JD.field "facelets" decodeFaceletsArray)
        (JD.field "state" decodeFaceletsEventState)
        (JD.field "timestamp" JD.int)


type alias GyroEvent =
    { timestamp : Int
    , quaternion : Quaternion
    , velocity :
        { x : Int
        , y : Int
        , z : Int
        }
    }


defaultGyroEvent : GyroEvent
defaultGyroEvent =
    { timestamp = 0
    , quaternion = Quaternion.quaternion { w = 1, x = 0, y = 0, z = 0 }
    , velocity = { x = 0, y = 0, z = 0 }
    }


decodeQuaternion : JD.Decoder Quaternion
decodeQuaternion =
    JD.map4 (\w x y z -> Quaternion.quaternion { w = w, x = x, y = y, z = z })
        (JD.field "w" JD.float)
        (JD.field "x" JD.float)
        (JD.field "y" JD.float)
        (JD.field "z" JD.float)


decodeVelocity : JD.Decoder { x : Int, y : Int, z : Int }
decodeVelocity =
    JD.map3 (\x y z -> { x = x, y = y, z = z })
        (JD.field "x" JD.int)
        (JD.field "y" JD.int)
        (JD.field "z" JD.int)


decodeGyroEvent : JD.Decoder GyroEvent
decodeGyroEvent =
    JD.map3 GyroEvent
        (JD.field "timestamp" JD.int)
        (JD.field "quaternion" decodeQuaternion)
        (JD.field "velocity" decodeVelocity)


getFace : Face -> Vector54.Vector54 Face -> Cube.CubeSide Face
getFace side facelets =
    case side of
        U ->
            Vector9.from9
                (Vector54.get Vector54.Index0 facelets)
                (Vector54.get Vector54.Index1 facelets)
                (Vector54.get Vector54.Index2 facelets)
                (Vector54.get Vector54.Index3 facelets)
                (Vector54.get Vector54.Index4 facelets)
                (Vector54.get Vector54.Index5 facelets)
                (Vector54.get Vector54.Index6 facelets)
                (Vector54.get Vector54.Index7 facelets)
                (Vector54.get Vector54.Index8 facelets)

        R ->
            Vector9.from9
                (Vector54.get Vector54.Index9 facelets)
                (Vector54.get Vector54.Index10 facelets)
                (Vector54.get Vector54.Index11 facelets)
                (Vector54.get Vector54.Index12 facelets)
                (Vector54.get Vector54.Index13 facelets)
                (Vector54.get Vector54.Index14 facelets)
                (Vector54.get Vector54.Index15 facelets)
                (Vector54.get Vector54.Index16 facelets)
                (Vector54.get Vector54.Index17 facelets)

        F ->
            Vector9.from9
                (Vector54.get Vector54.Index18 facelets)
                (Vector54.get Vector54.Index19 facelets)
                (Vector54.get Vector54.Index20 facelets)
                (Vector54.get Vector54.Index21 facelets)
                (Vector54.get Vector54.Index22 facelets)
                (Vector54.get Vector54.Index23 facelets)
                (Vector54.get Vector54.Index24 facelets)
                (Vector54.get Vector54.Index25 facelets)
                (Vector54.get Vector54.Index26 facelets)

        D ->
            Vector9.from9
                (Vector54.get Vector54.Index27 facelets)
                (Vector54.get Vector54.Index28 facelets)
                (Vector54.get Vector54.Index29 facelets)
                (Vector54.get Vector54.Index30 facelets)
                (Vector54.get Vector54.Index31 facelets)
                (Vector54.get Vector54.Index32 facelets)
                (Vector54.get Vector54.Index33 facelets)
                (Vector54.get Vector54.Index34 facelets)
                (Vector54.get Vector54.Index35 facelets)

        L ->
            Vector9.from9
                (Vector54.get Vector54.Index36 facelets)
                (Vector54.get Vector54.Index37 facelets)
                (Vector54.get Vector54.Index38 facelets)
                (Vector54.get Vector54.Index39 facelets)
                (Vector54.get Vector54.Index40 facelets)
                (Vector54.get Vector54.Index41 facelets)
                (Vector54.get Vector54.Index42 facelets)
                (Vector54.get Vector54.Index43 facelets)
                (Vector54.get Vector54.Index44 facelets)

        B ->
            Vector9.from9
                (Vector54.get Vector54.Index45 facelets)
                (Vector54.get Vector54.Index46 facelets)
                (Vector54.get Vector54.Index47 facelets)
                (Vector54.get Vector54.Index48 facelets)
                (Vector54.get Vector54.Index49 facelets)
                (Vector54.get Vector54.Index50 facelets)
                (Vector54.get Vector54.Index51 facelets)
                (Vector54.get Vector54.Index52 facelets)
                (Vector54.get Vector54.Index53 facelets)


cubeFromEvent : FaceletsEvent -> Cube Face
cubeFromEvent { facelets } =
    { u = getFace U facelets
    , l = getFace L facelets
    , f = getFace F facelets
    , r = getFace R facelets
    , b = getFace B facelets
    , d = getFace D facelets
    }
