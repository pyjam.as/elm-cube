module Levels exposing (allLevels)

import Cube exposing (Cube, Face(..), emptyCube, solvedCube)
import Game exposing (Level, TimedTarget(..))
import Vector9


allLevels : Game.LevelSelect
allLevels =
    { before = []
    , selected = yo
    , after =
        [ testLevel
        , movez
        , slightlyMoreInterestingTestLevel
        , testLevel
        , jPerms
        ]
    }


testLevel : Level
testLevel =
    { title = "TestLevel"
    , targets =
        [ TimedTarget 1000 (Cube.map Just Cube.solvedCube)
        , TimedTarget 2000 (Cube.map Just Cube.solvedCube)
        , TimedTarget 2500 (Cube.map Just Cube.solvedCube)
        ]
    }


movez : Level
movez =
    { title = "Movez"
    , targets =
        [ TimedTarget 5000
            { emptyCube
                | u =
                    emptyCube.u
                        |> Vector9.set Vector9.Index8 (Just F)
            }
        , TimedTarget 6000
            { emptyCube
                | l =
                    emptyCube.l
                        |> Vector9.set Vector9.Index2 (Just D)
            }
        , TimedTarget 7000
            { emptyCube
                | f =
                    emptyCube.f
                        |> Vector9.set Vector9.Index2 (Just U)
            }
        , TimedTarget 8000
            { emptyCube
                | r =
                    emptyCube.r
                        |> Vector9.set Vector9.Index0 (Just R)
            }
        , TimedTarget 9000
            { emptyCube
                | u =
                    emptyCube.u
                        |> Vector9.set Vector9.Index8 (Just F)
            }
        , TimedTarget 10000
            { emptyCube
                | l =
                    emptyCube.l
                        |> Vector9.set Vector9.Index2 (Just R)
            }
        , TimedTarget 11000
            { emptyCube
                | f =
                    emptyCube.f
                        |> Vector9.set Vector9.Index2 (Just U)
            }
        , TimedTarget 12000
            { emptyCube
                | r =
                    emptyCube.r
                        |> Vector9.set Vector9.Index0 (Just U)
            }
        , TimedTarget 15000 <| Cube.map Just Cube.solvedCube
        ]
    }


jPerms : Level
jPerms =
    { title = "J-Perms"
    , targets =
        [ TimedTarget 3500
            { emptyCube
                | f =
                    emptyCube.f
                        |> Vector9.set Vector9.Index0 (Just B)
                        |> Vector9.set Vector9.Index1 (Just F)
                        |> Vector9.set Vector9.Index2 (Just F)
                , l =
                    emptyCube.l
                        |> Vector9.set Vector9.Index0 (Just F)
                        |> Vector9.set Vector9.Index1 (Just R)
                        |> Vector9.set Vector9.Index2 (Just R)
                , b =
                    emptyCube.b
                        |> Vector9.set Vector9.Index0 (Just L)
                        |> Vector9.set Vector9.Index1 (Just L)
                        |> Vector9.set Vector9.Index2 (Just L)
                , r =
                    emptyCube.r
                        |> Vector9.set Vector9.Index0 (Just R)
                        |> Vector9.set Vector9.Index1 (Just B)
                        |> Vector9.set Vector9.Index2 (Just B)
            }
        , TimedTarget 6000
            { emptyCube
                | f =
                    emptyCube.f
                        |> Vector9.set Vector9.Index0 (Just L)
                        |> Vector9.set Vector9.Index1 (Just F)
                        |> Vector9.set Vector9.Index2 (Just F)
                , l =
                    emptyCube.l
                        |> Vector9.set Vector9.Index0 (Just B)
                        |> Vector9.set Vector9.Index1 (Just B)
                        |> Vector9.set Vector9.Index2 (Just B)
                , b =
                    emptyCube.b
                        |> Vector9.set Vector9.Index0 (Just F)
                        |> Vector9.set Vector9.Index1 (Just R)
                        |> Vector9.set Vector9.Index2 (Just R)
                , r =
                    emptyCube.r
                        |> Vector9.set Vector9.Index0 (Just R)
                        |> Vector9.set Vector9.Index1 (Just L)
                        |> Vector9.set Vector9.Index2 (Just L)
            }
        , TimedTarget 8500 <| Cube.map Just Cube.solvedCube
        ]
    }


yo : Level
yo =
    { title = "Yo"
    , targets =
        [ TimedTarget 5000
            { emptyCube
                | b =
                    emptyCube.b
                        |> Vector9.set Vector9.Index0 (Just F)
                        |> Vector9.set Vector9.Index1 (Just F)
                        |> Vector9.set Vector9.Index2 (Just F)
                        |> Vector9.set Vector9.Index6 (Just F)
                        |> Vector9.set Vector9.Index7 (Just F)
                        |> Vector9.set Vector9.Index8 (Just F)
                , f =
                    emptyCube.f
                        |> Vector9.set Vector9.Index0 (Just B)
                        |> Vector9.set Vector9.Index1 (Just B)
                        |> Vector9.set Vector9.Index2 (Just B)
                        |> Vector9.set Vector9.Index6 (Just B)
                        |> Vector9.set Vector9.Index7 (Just B)
                        |> Vector9.set Vector9.Index8 (Just B)
            }
        , TimedTarget 8000
            { emptyCube
                | b =
                    emptyCube.b
                        |> Vector9.set Vector9.Index0 (Just B)
                        |> Vector9.set Vector9.Index1 (Just F)
                        |> Vector9.set Vector9.Index2 (Just B)
                        |> Vector9.set Vector9.Index6 (Just B)
                        |> Vector9.set Vector9.Index7 (Just F)
                        |> Vector9.set Vector9.Index8 (Just B)
                , f =
                    emptyCube.f
                        |> Vector9.set Vector9.Index0 (Just F)
                        |> Vector9.set Vector9.Index1 (Just B)
                        |> Vector9.set Vector9.Index2 (Just F)
                        |> Vector9.set Vector9.Index6 (Just F)
                        |> Vector9.set Vector9.Index7 (Just B)
                        |> Vector9.set Vector9.Index8 (Just F)
            }
        , TimedTarget 11000
            { emptyCube
                | u =
                    emptyCube.u
                        |> Vector9.set Vector9.Index0 (Just U)
                        |> Vector9.set Vector9.Index2 (Just U)
                        |> Vector9.set Vector9.Index4 (Just U)
                        |> Vector9.set Vector9.Index6 (Just U)
                        |> Vector9.set Vector9.Index8 (Just U)
                , d =
                    emptyCube.f
                        |> Vector9.set Vector9.Index0 (Just D)
                        |> Vector9.set Vector9.Index2 (Just D)
                        |> Vector9.set Vector9.Index4 (Just D)
                        |> Vector9.set Vector9.Index6 (Just D)
                        |> Vector9.set Vector9.Index8 (Just D)
            }
        ]
    }


slightlyMoreInterestingTestLevel : Level
slightlyMoreInterestingTestLevel =
    { title = "SMIL"
    , targets =
        [ TimedTarget 5000
            { emptyCube
                | r =
                    emptyCube.r
                        |> Vector9.set Vector9.Index1 (Just F)
                        |> Vector9.set Vector9.Index2 (Just F)
            }
        , TimedTarget 7000
            { emptyCube
                | r =
                    emptyCube.r
                        |> Vector9.set Vector9.Index5 (Just F)
                        |> Vector9.set Vector9.Index8 (Just F)
            }
        , TimedTarget 9000
            { emptyCube
                | u =
                    emptyCube.u
                        |> Vector9.set Vector9.Index1 (Just F)
                        |> Vector9.set Vector9.Index2 (Just F)
            }
        , TimedTarget 11000
            { emptyCube
                | u =
                    emptyCube.u
                        |> Vector9.set Vector9.Index0 (Just F)
                        |> Vector9.set Vector9.Index3 (Just F)
            }
        , TimedTarget 18000 <| Cube.map Just Cube.solvedCube
        ]
    }
