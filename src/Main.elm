module Main exposing (main)

import Angle
import Axis3d
import Block3d
import Browser
import Browser.Events
import Camera3d
import Color
import Cube exposing (Cube, CubeSide, Face(..), emptyCube)
import Direction3d
import Element as El
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Game exposing (Game)
import GanCube
import Html exposing (Html)
import Json.Decode as JD
import Length
import Levels
import Pixels
import Point3d
import Quaternion exposing (Quaternion)
import Scene3d
import Scene3d.Material as Material
import Time
import Vector3d
import Vector9
import Viewpoint3d


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions =
            \_ ->
                Sub.batch
                    [ GanCube.getCubeEvents NewCubeEvent
                    , Browser.Events.onAnimationFrame Tick
                    , Browser.Events.onResize OnResize
                    ]
        }



-- MODEL


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { now = Time.millisToPosix 0
      , moves = []
      , gyro = GanCube.defaultGyroEvent
      , gyroOffset = Quaternion.quaternion { w = 1, x = 0, y = 0, z = 0 }
      , currentGyro = Quaternion.quaternion { w = 1, x = 0, y = 0, z = 0 }
      , cube = Cube.solvedCube
      , game = Nothing
      , levelSelect = Levels.allLevels
      , width = flags.w
      , height = flags.h
      }
    , Cmd.none
    )


type alias Model =
    { now : Time.Posix
    , moves : List GanCube.MoveEvent
    , gyro : GanCube.GyroEvent
    , gyroOffset : Quaternion
    , currentGyro : Quaternion
    , cube : Cube Face
    , game : Maybe Game
    , levelSelect : Game.LevelSelect
    , width : Int
    , height : Int
    }


type alias Flags =
    { w : Int
    , h : Int
    }



-- MSG / UPDATE


type Msg
    = -- data from browser
      Tick Time.Posix
    | OnResize Int Int
      -- data from cube (via js port)
    | NewCubeEvent (Result JD.Error GanCube.CubeEvent)
      -- buttons
    | ConnectCubeClicked
    | ResetCubeClicked
    | RequestFaceletsClicked


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick now ->
            ( { model
                | now = now
                , game = Maybe.map (Game.updateGame now model.cube) model.game
                , currentGyro =
                    model.currentGyro
                        |> Quaternion.moveTowards (Quaternion.mul model.gyro.quaternion (Quaternion.conjugate model.gyroOffset)) 0.5
              }
            , Cmd.none
            )

        NewCubeEvent (Ok (GanCube.Move moveEvent)) ->
            ( let
                newMoves =
                    moveEvent :: model.moves

                latestFourMoves =
                    List.take 4 newMoves |> List.map .face

                -- cube controls for starting the game
                newGame : Maybe Game
                newGame =
                    case ( model.game, latestFourMoves ) of
                        ( Nothing, [ U, U, U, U ] ) ->
                            Just <| Game.startGame model.now model.levelSelect.selected

                        ( Just game, [ U, U, U, U ] ) ->
                            if Game.isFinished game then
                                Just <| Game.startGame model.now model.levelSelect.selected

                            else
                                model.game

                        _ ->
                            model.game

                -- cube controls for calibrating gyro
                newGyroOffset : Quaternion
                newGyroOffset =
                    case latestFourMoves of
                        [ D, D, D, D ] ->
                            model.gyro.quaternion

                        _ ->
                            model.gyroOffset

                -- cube controls for going back from a finished game
                goBack : Bool
                goBack =
                    case ( model.game, latestFourMoves ) of
                        ( Just game, [ B, B, B, B ] ) ->
                            Game.isFinished game

                        _ ->
                            False

                -- cube controls for level select
                newLevelSelect =
                    case ( model.game, moveEvent.move ) of
                        ( Nothing, Cube.Move R Cube.Clockwise ) ->
                            Game.selectPrevious model.levelSelect

                        ( Nothing, Cube.Move R Cube.CounterClockwise ) ->
                            Game.selectNext model.levelSelect

                        _ ->
                            model.levelSelect
              in
              { model
                | moves = newMoves
                , game =
                    if goBack then
                        Nothing

                    else
                        newGame
                , gyroOffset = newGyroOffset
                , levelSelect = newLevelSelect
              }
            , GanCube.requestFacelets
            )

        NewCubeEvent (Ok (GanCube.Gyro gyroEvent)) ->
            ( { model | gyro = gyroEvent }, Cmd.none )

        NewCubeEvent (Ok (GanCube.Facelets faceletEvent)) ->
            ( { model | cube = GanCube.cubeFromEvent faceletEvent }, Cmd.none )

        NewCubeEvent (Err _) ->
            -- TODO: display error
            ( model, Cmd.none )

        ConnectCubeClicked ->
            ( model
            , GanCube.connectCube ()
            )

        ResetCubeClicked ->
            ( model
            , GanCube.requestReset
            )

        RequestFaceletsClicked ->
            ( model
            , GanCube.requestFacelets
            )

        OnResize x y ->
            ( { model | width = x, height = y }, Cmd.none )



-- 3D STUFF


movementCurve : Float -> Float
movementCurve x =
    let
        base =
            2

        xn =
            x / 300
    in
    xn / 10 + 3 * ((base ^ xn / (1 + base ^ xn)) - 0.5)


targetEntities : Game.GameTimeMillis -> Game.TimedTarget -> Cube (Maybe (Scene3d.Entity coordinates))
targetEntities now (Game.TimedTarget time targetCube) =
    let
        -- TODO translate away by time until target event
        distance =
            movementCurve (toFloat (time - now))

        blocks =
            translateAwayFromCubeBy distance defaultFaceletBlocks

        maybeColors =
            Cube.map (Maybe.map Cube.faceToColor) targetCube
    in
    Cube.map2
        (\block maybeColor -> Maybe.map (\color -> Scene3d.blockWithShadow (Material.matte color) block) maybeColor)
        blocks
        maybeColors


cubeEntities : Cube Face -> Cube (Scene3d.Entity coordinates)
cubeEntities cubeFaces =
    cubeFaces
        |> Cube.map Cube.faceToColor
        |> Cube.map2 (\block color -> Scene3d.blockWithShadow (Material.matte color) block) defaultFaceletBlocks


translateAwayFromCubeBy : Float -> Cube (Block3d.Block3d Length.Meters coordinates) -> Cube (Block3d.Block3d Length.Meters coordinates)
translateAwayFromCubeBy length cube =
    let
        translate by =
            Vector9.map (Block3d.translateBy by)
    in
    { u = translate (Vector3d.meters 0 0 length) cube.u
    , l = translate (Vector3d.meters -length 0 0) cube.l
    , f = translate (Vector3d.meters 0 -length 0) cube.f
    , r = translate (Vector3d.meters length 0 0) cube.r
    , b = translate (Vector3d.meters 0 length 0) cube.b
    , d = translate (Vector3d.meters 0 0 -length) cube.d
    }


defaultFaceletBlocks : Cube (Block3d.Block3d Length.Meters coordinates)
defaultFaceletBlocks =
    let
        rotate axis angle =
            Vector9.map (Block3d.rotateAround axis angle) cubeSideFBlocks
    in
    { u = rotate Axis3d.x (Angle.degrees -90)
    , l = rotate Axis3d.z (Angle.degrees -90)
    , f = cubeSideFBlocks
    , r = rotate Axis3d.z (Angle.degrees 90)
    , b = rotate Axis3d.z (Angle.degrees 180)
    , d = rotate Axis3d.x (Angle.degrees 90)
    }


cubeSideFBlocks : CubeSide (Block3d.Block3d Length.Meters coordinates)
cubeSideFBlocks =
    -- facelets for one side of the cube
    let
        thickness =
            0.07

        padding =
            0.02

        dist =
            2 / 3

        topLeftBlock =
            Block3d.with
                { x1 = Length.meters (-1 + padding)
                , y1 = Length.meters -1
                , z1 = Length.meters (1 - padding)
                , x2 = Length.meters ((-1 + dist) - padding)
                , y2 = Length.meters (-1 - thickness)
                , z2 = Length.meters ((1 - dist) + padding)
                }

        makeFacelet : Vector3d.Vector3d Length.Meters coordinates -> Block3d.Block3d Length.Meters coordinates
        makeFacelet vec =
            Block3d.translateBy vec topLeftBlock

        topLeft =
            makeFacelet (Vector3d.meters 0 0 0)

        top =
            makeFacelet (Vector3d.meters dist 0 0)

        topRight =
            makeFacelet (Vector3d.meters (2 * dist) 0 0)

        left =
            makeFacelet (Vector3d.meters 0 0 -dist)

        center =
            makeFacelet (Vector3d.meters dist 0 -dist)

        right =
            makeFacelet (Vector3d.meters (2 * dist) 0 -dist)

        bottomLeft =
            makeFacelet (Vector3d.meters 0 0 (-2 * dist))

        bottom =
            makeFacelet (Vector3d.meters dist 0 (-2 * dist))

        bottomRight =
            makeFacelet (Vector3d.meters (2 * dist) 0 (-2 * dist))
    in
    Vector9.from9
        topLeft
        top
        topRight
        left
        center
        right
        bottomLeft
        bottom
        bottomRight


platform : Scene3d.Entity coordinates
platform =
    let
        z =
            -8

        dxy =
            300

        thickness =
            0.2
    in
    Scene3d.blockWithShadow (Material.matte (Color.rgb 0.1 0.1 0.1)) <|
        Block3d.with
            { x1 = Length.meters <| -1 - dxy
            , y1 = Length.meters <| -1 - dxy
            , z1 = Length.meters z
            , x2 = Length.meters <| 1 + dxy
            , y2 = Length.meters <| 1 + dxy
            , z2 = Length.meters <| z - thickness
            }


camera : Camera3d.Camera3d Length.Meters coordinates
camera =
    Camera3d.perspective
        { viewpoint =
            Viewpoint3d.lookAt
                { eyePoint = Point3d.meters 0 -10 5
                , focalPoint = Point3d.origin
                , upDirection = Direction3d.positiveZ
                }
        , verticalFieldOfView = Angle.degrees 40
        }


view3d : Model -> Html msg
view3d model =
    let
        currentCube =
            cubeEntities model.cube |> Cube.toList

        currentTarget =
            case model.game of
                Just game ->
                    game.level.targets
                        |> List.map (targetEntities (Game.gametime game model.now))
                        |> List.map Cube.toList
                        |> List.concat
                        |> List.filterMap identity

                _ ->
                    []

        rotationQuaternion =
            model.currentGyro

        rotated =
            List.map
                (\facelet ->
                    Scene3d.rotateAround
                        (Quaternion.axisOfRotation rotationQuaternion)
                        (Quaternion.angleOfRotation rotationQuaternion)
                        facelet
                )
                (currentCube ++ currentTarget)
    in
    Scene3d.sunny
        { entities = platform :: rotated
        , camera = camera
        , upDirection = Direction3d.positiveZ
        , sunlightDirection = Direction3d.yz (Angle.degrees -65)
        , shadows = True
        , clipDepth = Length.meters 1
        , background = Scene3d.transparentBackground
        , dimensions = ( Pixels.pixels model.width, Pixels.pixels model.height )
        }



-- VIEW


cubeConnected : Model -> Bool
cubeConnected model =
    model.gyro /= GanCube.defaultGyroEvent || model.moves /= []


view : Model -> Html Msg
view model =
    let
        genericButton : String -> Msg -> El.Element Msg
        genericButton text msg =
            Input.button
                [ El.padding 10
                , Background.color (El.rgb 0.9 0.9 0.9)
                , Border.rounded 5
                ]
                { onPress = Just msg
                , label = El.text text
                }

        buttons =
            El.row [ El.spacing 10, El.padding 10, El.alignBottom ]
                [ genericButton "Reset as solved" ResetCubeClicked
                , genericButton "Request cube state" RequestFaceletsClicked
                ]

        pointsDisplay : Game -> El.Element msg
        pointsDisplay game =
            let
                hitText =
                    "Hit: " ++ String.fromInt game.goodCount

                missedText =
                    "Missed: " ++ String.fromInt game.badCount
            in
            El.column
                [ El.alignTop
                , El.alignLeft
                , Font.size 50
                , Font.color (El.rgb 1 1 1)
                , El.spacing 20
                , El.padding 24
                ]
                [ El.text hitText
                , El.text missedText
                ]

        winScreen : Game -> El.Element msg
        winScreen game =
            if Game.isFinished game then
                let
                    accuracy : Float
                    accuracy =
                        toFloat game.goodCount / toFloat (game.goodCount + game.badCount)

                    accuracyPercent : Int
                    accuracyPercent =
                        round (accuracy * 100)
                in
                El.column
                    [ El.centerX
                    , El.centerY
                    , El.padding 30
                    , El.spacing 30
                    , Background.color <| El.rgb 1 1 1
                    , Border.rounded 10
                    , Border.shadow
                        { offset = ( 2.5, 2.5 )
                        , size = 2
                        , blur = 10
                        , color = El.rgb 0 0 0
                        }
                    ]
                    [ El.el [ Font.size 54, El.centerX ] <| El.text "Game Over"
                    , El.el [ Font.size 45, El.centerX ] <| El.text <| "Accuracy: " ++ String.fromInt accuracyPercent ++ "%"
                    , El.el [ Font.size 35, El.centerX ] <| El.text <| "Do U4 to play again"
                    , El.el [ Font.size 35, El.centerX ] <| El.text <| "Do B4 to go back"
                    ]

            else
                El.text ""

        connectCubeDialog =
            if cubeConnected model then
                El.text ""

            else
                El.column
                    [ El.centerX
                    , El.centerY
                    , El.padding 20
                    , El.spacing 20
                    , Border.rounded 5
                    , Background.color (El.rgb 1 1 1)
                    ]
                    [ El.text "No cube connected"
                    , El.el [ El.centerX ] <| genericButton "Connect Cube" ConnectCubeClicked
                    ]

        levelSelectDialog =
            let
                levelText : Game.Level -> El.Element msg
                levelText level =
                    El.el
                        [ Font.color <| El.rgb 0.4 0.4 0.4
                        , Font.size 30
                        , El.alignRight
                        ]
                    <|
                        El.text level.title

                element =
                    El.column
                        [ El.alignRight
                        , Font.alignRight
                        , El.centerY
                        , Font.color <| El.rgb 1 1 1
                        , El.padding 30
                        , El.padding 10
                        ]
                        [ El.column [ El.alignRight ] (List.map levelText model.levelSelect.before)
                        , El.el [ Font.size 50, El.alignRight ] <| El.text model.levelSelect.selected.title
                        , El.column [ El.alignRight ] (List.map levelText model.levelSelect.after)
                        ]
            in
            case ( model.game, cubeConnected model ) of
                -- show level select if no game is running, but the cube is connected.
                ( Nothing, True ) ->
                    element

                _ ->
                    El.text ""
    in
    El.layout [] <|
        El.el
            [ Maybe.map pointsDisplay model.game
                |> Maybe.withDefault (El.text "")
                |> El.inFront
            , Maybe.map winScreen model.game
                |> Maybe.withDefault (El.text "")
                |> El.inFront
            , buttons
                |> El.inFront
            , connectCubeDialog
                |> El.inFront
            , levelSelectDialog
                |> El.inFront
            ]
        <|
            El.html
                (view3d model)
