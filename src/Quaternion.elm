module Quaternion exposing (Quaternion, angleOfRotation, axisOfRotation, conjugate, moveTowards, mul, quaternion)

import Angle
import Area
import Axis3d
import Length
import Point3d
import Vector3d


type alias Quaternion =
    { vector : Vector3d.Vector3d Length.Meters Float
    , scalar : Float
    }


{-| Construct a quaternion from its four components
-}
quaternion : { w : Float, x : Float, y : Float, z : Float } -> Quaternion
quaternion { w, x, y, z } =
    { vector = Vector3d.meters x y z
    , scalar = w
    }


{-| Multiply two quaternions.
-}
mul : Quaternion -> Quaternion -> Quaternion
mul p q =
    { scalar = q.scalar * p.scalar - Area.inSquareMeters (Vector3d.dot q.vector p.vector)
    , vector =
        Vector3d.multiplyBy q.scalar p.vector
            |> Vector3d.plus (Vector3d.multiplyBy p.scalar q.vector)
            |> Vector3d.plus (Vector3d.fromTuple Length.meters (Vector3d.toTuple Area.inSquareMeters (Vector3d.cross p.vector q.vector)))
    }


conjugate : Quaternion -> Quaternion
conjugate q =
    { scalar = q.scalar, vector = Vector3d.multiplyBy -1 q.vector }


angleOfRotation : Quaternion -> Angle.Angle
angleOfRotation q =
    let
        halfTurn =
            (Vector3d.length q.vector |> Length.inMeters) / q.scalar

        s =
            asin (2 * halfTurn / (1 + halfTurn ^ 2))
    in
    if q.scalar == 0 then
        Angle.radians pi

    else if abs halfTurn < 1 then
        Angle.radians s

    else
        Angle.radians <| pi - s


{-| Get the axis of rotation for a quaternion. Defaults to the x axis if there is no rotation.
-}
axisOfRotation : Quaternion -> Axis3d.Axis3d Length.Meters Float
axisOfRotation q =
    let
        dir =
            Vector3d.direction (Vector3d.normalize q.vector)
    in
    case dir of
        Nothing ->
            Axis3d.x

        Just something ->
            Axis3d.through Point3d.origin something


moveTowards : Quaternion -> Float -> Quaternion -> Quaternion
moveTowards target distance source =
    let
        scalar =
            (distance * target.scalar) + ((1 - distance) * source.scalar)

        x =
            distance * Length.inMeters (Vector3d.xComponent target.vector) + (1 - distance) * Length.inMeters (Vector3d.xComponent source.vector)

        y =
            distance * Length.inMeters (Vector3d.yComponent target.vector) + (1 - distance) * Length.inMeters (Vector3d.yComponent source.vector)

        z =
            distance * Length.inMeters (Vector3d.zComponent target.vector) + (1 - distance) * Length.inMeters (Vector3d.zComponent source.vector)
    in
    { source
        | scalar = scalar
        , vector = Vector3d.meters x y z
    }
